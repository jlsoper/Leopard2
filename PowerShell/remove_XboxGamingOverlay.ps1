#!/usr/bin/env pwsh

Get-AppxPackage -AllUsers Microsoft.XboxGamingOverlay | Remove-AppxPackage

Write-Host "Microsoft XboxGamingOverlay has been removed!"

