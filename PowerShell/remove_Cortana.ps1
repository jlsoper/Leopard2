#!/usr/bin/env pwsh


Get-AppxPackage *Microsoft.549981C3F5F10* | Remove-AppxPackage
Get-AppxPackage -AllUsers -PackageTypeFilter Bundle -name "*Microsoft.549981C3F5F10*" | Remove-AppxPackage -AllUsers

Write-Host "Microsoft Cortana has been removed!"

